#!/usr/bin/perl

# ChatNetBouncer: a bouncer and IRC bridge for the ChatNet protocol
#

# todo:
# allow chatnet clients to ping this bouncer for player count
# queue for ToServer
# IRC: remove /me
# IRC: pidgin crashes
# different max length with cross zone PMs

use v5.8;
use strict;
use warnings;
use sigtrap;
use Socket;
use IO::Socket;
use Tie::RefHash;
use Carp;
use IO::Socket::INET;
use IO::Select;
use Data::Dumper;
use Text::Wrap;
use Time::HiRes qw (time alarm usleep);   # Need floating points for time() and sleep()
use Encode;
use Encode::Byte;
use constant ASSS_MAXMESSAGELENGTH => 249; # chat.c:153 ASSS 1.4.4

my $defaultconfig = 
{
        server_ip => '208.122.59.226',     # IP/Hostname of the server
        server_port => '5005',             # Port of the server
        server_arena => '0',               # Arena to join
        
        server_reconnectInitialDelay => 5, # How many seconds to wait after a disconnect
        server_reconnectDelay => 20,       # How many seconds to wait between reattempting to connect after being unable to establish a connection
        server_pingInterval => 10,         # How often to send a ping to the server
        server_timeout => 30,              # When to consider connection to server to have timed out. Make sure pinginterval is much lower then this
        
        server_logRaw => 0,                # Log incomming and outgoing raw chatnet messages
        server_chat_refreshdelay => 0,     # How often to update ?chat. This is only useful when you connect with an irc client and use /join #mychat
        
        nick => 'JoWie',                   # Subspace nickname
        password => '',                    # Password
        chat => '',                        # ?chat= to send upon join
        
        listen_bind => '0.0.0.0',          # '0.0.0.0' for anyone, use '127.0.0.1' if you only want to connect locally
        listen_port => '5005',             # Port to listen for connections
        listen_password => '',             # If set, a different password is required for connecting towards the bouncer
        
        client_loginTimeout => 30,         # How long has the client to succesfully login before closing his socket?
        client_logRaw => 0,
        client_ignoreGo => 1,              # Ignore GO: from the client
        client_noopInterval => 10,         # How often to send a NOOP to the client
        client_bouncerCommand => 'bnc',    # What command to use for bouncer specific messages
        
        client_awayAfterInactivity => 300, # After how many seconds of inactivity to begin logging messages.
        client_awayAfterDisconnect => 1,   # Sets you as away when you disconnect from the bouncer
        client_awayMessage => 'Away, your message has been logged',
        client_awayLogFile => 'away.txt',
        client_awayIgnore => ['idlerpg'],  # Players to never log messages from, or send an auto reply, keep this list lowercase. Messages you send to yourself are never logged
        
        client_ignoreList => 'ignore.txt',
        client_allowCommands => 1,
        client_allowTokens => 1,
        
        client_remotePrefix => 0, # add a prefix to messages comming from outside the arena?
        client_doubleJoinedChatsMsgs => 0, # if this is set, any ?chat message that you have joined (/join #irpg) is also sent to &ChatNet
        client_autoJoinChats => 0, # if this is set, you will automatically join any ?chat's you are on in irc mode
        client_ircUTF8 => 1, # convert UTF-8 to Windows-1252
        client_ircNickServEmulation => 1, # accept /msg nickserv identify password instead of /pass
        client_ircColors => 1,
        client_allowEval => 1,
        
};
my $cfgFile = "bnc.conf";
$cfgFile = $ARGV[0] if (defined($ARGV[0]));
print("Reading config from file $cfgFile...\n");
my $userconfig = do $cfgFile;
die("Unable to load config file: $!") unless (defined($userconfig));
my %config = (%$defaultconfig, %$userconfig);

my $ircSpace = '_';

my $select = IO::Select->new();

my $server_sock = 0; # socket in %sockets that has SOCKETTYPE_SERVER
my $client_sock = 0; # socket in %sockets that has SOCKETTYPE_CLIENT
my $listen_sock = 0; # # socket in %sockets that has SOCKETTYPE_LISTEN

my $server_nick = $config{nick}; # Current nickname as connected to server (response from LOGINOK)
my $server_nicklc = lc($server_nick);
my $server_ircNick = ircNick($server_nick);
my $server_ircNicklc = lc($server_ircNick);
my $server_arena = $config{server_arena}; # Arena we are currently in
my $server_freq = -1;
my $server_billerDown = 0;

# Player list of the current arena
# $server_players{'lowercase chatnet nick'}{nick}
# $server_players{'lowercase chatnet nick'}{irc_nick}
# $server_players{'lowercase chatnet nick'}{ship}
# $server_players{'lowercase chatnet nick'}{freq}
# $server_players{'lowercase chatnet nick'}{enter_count}
my %server_players = ();
my @server_chats = ();
my %server_chatsR = ();
my %server_playersOnChat = ();
my $server_playersOnChat_commandSent = 0;
my $server_playersOnChat_receivedLines = 0;
my $server_playersOnChat_lastUpdate = time();
my %server_playersOnChat_buffer = ();

my $server_nextConnectionAttempt = time();

use constant CLIENTMODE_CHATNET => 0;
use constant CLIENTMODE_IRC   => 1;
my $client_mode = CLIENTMODE_CHATNET;
my $client_awayMessage = $config{client_awayMessage};
my $client_lastActivity = 0;
my $client_JACC1_2Fixes = 0;
my $client_isAway = 0;
my $client_ircMode_gatheringPlayers = 0; # Used by ChatNetToIrc, if an INARENA is received this is set to 1. If a message is received that does not begin with PLAYER set this to 0 and send the player list to the irc client
my $client_ircMode_isInChannel = 0;
my $client_ircMode_ison_startedAt = 0;
my $client_ircMode_ison_findsLeft = 0;
my @client_ircMode_ison = ();
my %client_ircMode_joinedChats = (); # value is always 1

tie my %sockets, 'Tie::RefHash'; # $sockets{$sock} = SocketInfo();

my %usedIrcNicks;  # All irc nicks used in this arena, this is used to avoid duplicate names. $usedIrcNicks{lc('IRC_Nick')} = 'ChatNet Nick' 

my $version = 'v2.0.0';
my %lastMessageLogged; # $lastMessageLogged{nickname} = time(); 
my @awaylogmessages = ();
my %ignoreList = (); #$ignoreList{lc('Chatnet Name')} = 'Chatnet Name';

use constant SOCKETTYPE_NONE   => 0;            # Error
use constant SOCKETTYPE_SERVER => 1;            # Connection to ChatNet Server
use constant SOCKETTYPE_LISTEN => 2;            # Listen socket
use constant SOCKETTYPE_CLIENT => 3;            # Connection to the client logged in
use constant SOCKETTYPE_CLIENTPENDING => 4;     # Clients not yet logged in

$SIG{INT} = 'sigint'; # Pressing CTRL-C in the console, closing console, etc
$SIG{TERM} = 'sigint';

#This work around is no longer needed in newer perl versions (5.10)
BEGIN 
{
        # Make IO::Socket no blocking work on windows
        if( $^O eq 'MSWin32' ) 
        {
                *EWOULDBLOCK = sub () { 10035 };
                *EINPROGRESS = sub () { 10036 };
                no warnings 'redefine';                
                *IO::Socket::blocking = sub 
                {
                        my ($self, $blocking) = @_;
                        my $nonblocking = $blocking ? "0" : "1";
                        ioctl($self, 0x8004667e, $nonblocking);
                };
        }
        else 
        {
                require Errno;
                import  Errno qw(EWOULDBLOCK EINPROGRESS);
        }
}

print "\n";
l("Chatnet bouncer starting...");
ReadClientLogMessagesFromFile() if (length($config{client_awayLogFile}) && -e $config{client_awayLogFile});
ReadIgnoreList() if (length($config{client_ignoreList}) && -e $config{client_ignoreList});
 
l("Listening on $config{listen_bind}:$config{listen_port}...");

$listen_sock = IO::Socket::INET->new( 
                LocalHost => $config{listen_bind}, 
                LocalPort => $config{listen_port}, 
                Proto => 'tcp', 
                Listen => 2, 
                Reuse => 1,
                Blocking => 0,
                Timeout => 10,
        );

if (!$listen_sock)
{
        fe("Unable to listen on $config{listen_bind}:$config{listen_port} ($!)");
        return; #unreachable
}

AddSocket($listen_sock, SOCKETTYPE_LISTEN);

while (1)
{
        # do something
        UpdateSockets();
        UpdateServer();
        UpdateClient();
        UpdatePendingClients();

        usleep(10 * 1000); # Rest for 10 milliseconds (1 tick in subspace)
}

sub AddSocket
{
        my ($sock, $type) = @_;
        $select->add($sock);
        return $sockets{$sock} = SocketInfo($sock, $type);
}

sub RemoveSocket
{
        my ($sock) = @_;
        unless (defined $sock)
        {
                delete $sockets{$sock};
                return;
        }
        
        my $socketInfo = $sockets{$sock};
        $sock = $socketInfo->{sock};
        
        $server_sock = 0 if ($socketInfo->{type} == SOCKETTYPE_SERVER);
        if ($socketInfo->{type} == SOCKETTYPE_CLIENT)
        {
                $client_sock = 0;
                if (length($config{server_arena}) > 0 &&  $server_arena ne $config{server_arena}) # move back to orignal arena
                {
                        ToServer("GO:$config{server_arena}");
                }
        }
        $listen_sock = 0 if ($socketInfo->{type} == SOCKETTYPE_LISTEN);
        delete $sockets{$sock};
        
        $select->remove($sock);
        $sock->close();
}

sub SocketInfo
{
        my ($sock, $type) = @_;
        if (!defined($type) || $type <= SOCKETTYPE_NONE || $type > SOCKETTYPE_CLIENTPENDING)
        {
                $type = SOCKETTYPE_NONE;
                w("SOCKETTYPE_NONE used"); 
        }
        
        return 
        { 
                sock => $sock,                  # The key of the hash table %sockets is not a valid object!
                sock_peername => $sock->peername(),
                type => $type,                  # SOCKETTYPE_*
        
                lastMessageReceived => 0,       # time()
                connectionEstablished => 0,     # time()
                nextPing => 0,                  # When to send a new PING to yourself for SERVER, or a NOOP for  CLIENT
                
                buffer => '',
                
                irc_nick_valid => 0, # Has the IRC client sent a valid NICK command?
                irc_pass_valid => 0, # Has the IRC client sent a valid PASS command?
        };
}

sub sigint
{
        BouncerMsg("*** Shutting down: Received SIGNAL");
        RemoveSocket($_) foreach (keys %sockets);

        fe("Recieved SIGNAL");
}

sub UpdateSockets
{
        my @ready = $select->can_read(0); # No blocking
        my ($sock, $buf);
        foreach $sock (@ready)
        {
                my $socketInfo = $sockets{$sock};
                
                if ($socketInfo->{type} == SOCKETTYPE_NONE)
                {
                        w("SOCKETTYPE_NONE");
                }
                elsif ($socketInfo->{type} == SOCKETTYPE_LISTEN)
                {
                        my $new = $sock->accept();
                        
                        unless ($new)
                        {
                                w("\$listen_socket->accept() returned 0");
                                next;
                        }
                        
                        l("Connection from ".$new->peerhost());
                        
                        $socketInfo = AddSocket($new, SOCKETTYPE_CLIENTPENDING);
                        $socketInfo->{lastMessageReceived} = $socketInfo->{connectionEstablished} = time();
                }
                else # All other sockets can receive actual data
                {
                        w("Unknown socket in select object!") if (!$socketInfo);
                        
                        $buf = '';
                        $sock->recv($buf, 512, 0);
                        
                        if (length($buf))
                        {
                                $buf =~ s/\r/\n/g;
                                $socketInfo->{buffer} .= $buf;
                                $socketInfo->{lastMessageReceived} = time();
                        }
                        else # disconnected
                        {
                                if ($socketInfo->{type} == SOCKETTYPE_SERVER)
                                {
                                        $server_nextConnectionAttempt = time() + $config{server_reconnectInitialDelay};
                                        BouncerMsg("*** Connection to server lost (socket closed)");
                                        l("Connection to server lost (socket closed)");
                                }
                                
                                if ($socketInfo->{type} == SOCKETTYPE_CLIENT)
                                {
                                        l("Connection to client closed.");
                                }
                        
                                RemoveSocket($sock);
                        }
                }
        }
}

sub UpdateClient
{
        return unless ($client_sock);
        my $time = time();
        my $socketInfo = $sockets{$client_sock};
        
        if ($time >= $socketInfo->{nextPing})
        {
                ToClient("NOOP");
                $socketInfo->{nextPing} = $time + $config{client_noopInterval};
        }
        
        if ($client_ircMode_ison_startedAt && $time >= $client_ircMode_ison_startedAt + 10)
        {
                ToClient("303 $server_ircNick :" . join(' ', @client_ircMode_ison), 1);
                @client_ircMode_ison = ();
                $client_ircMode_ison_findsLeft = 0;
                $client_ircMode_ison_startedAt = 0;
        }
        
        if (length($socketInfo->{buffer}))
        {
                while (index($socketInfo->{buffer}, "\n") != -1)
                {
                        my $line = substr($socketInfo->{buffer}, 0, index($socketInfo->{buffer}, "\n") + 1);
                        $socketInfo->{buffer} = substr($socketInfo->{buffer}, length($line));
                        chomp($line);
                        if (length($line))
                        {
                                l("C << $line") if ($config{client_logRaw});
                                
                                if ($client_mode == CLIENTMODE_CHATNET)
                                {
                                        ClientParseMessageChatNet($line);
                                }
                                elsif ($client_mode == CLIENTMODE_IRC)
                                {
                                        if ($config{client_ircUTF8})
                                        {
                                                Encode::from_to($line, "utf8", "cp1252");
                                        }
                                
                                        ClientParseMessageIRC($line);
                                }
                        }
                }
        }
}

sub UpdateServer
{
        my $time = time();
        unless ($server_sock)
        {
                if ($time >= $server_nextConnectionAttempt)
                {
                        l("Connecting to $config{server_ip}:$config{server_port}...");
                        BouncerMsg("*** Connecting to $config{server_ip}:$config{server_port}...");
                        $server_sock = IO::Socket::INET->new((
                                PeerAddr        => $config{server_ip} .':'. $config{server_port} ,
                                Proto           => 'tcp',
                                Blocking        => 0,
                                Timeout         => 10,
                        ));
                        
                        
                        unless ($server_sock)
                        {
                                l("Unable to connect to the server");
                                BouncerMsg("*** Unable to connect to the server...");
                                $server_nextConnectionAttempt = time() + $config{server_reconnectDelay};
                        }
                        else
                        {
                                my $socketInfo = AddSocket($server_sock, SOCKETTYPE_SERVER);
                                $socketInfo->{lastMessageReceived} = $socketInfo->{connectionEstablished} = time();
                                $socketInfo->{nextPing} = $time + $config{server_pingInterval};
                                $server_playersOnChat_lastUpdate = $time;
                                
                                l("Connected to server (" . $server_sock->peerhost() . ":". $server_sock->peerport() . ")...");
                                
                                ToServer("LOGIN:1;ChatNetBouncer by JoWie:$config{nick}:$config{password}", "LOGIN:1;ChatNetBouncer by JoWie:$config{nick}:****************");
                        }
                }
        }
        else
        {
                my $socketInfo = $sockets{$server_sock}; 
                if ($time >= $socketInfo->{nextPing})
                {
                        ToServer("SEND:PRIV:$server_nick:PING:$time");
                        $socketInfo->{nextPing} = $time + $config{server_pingInterval};
                }
                
                if ($time > $socketInfo->{lastMessageReceived} + $config{server_timeout})
                {
                        l("Connection to server timed out.... Reconnecting in $config{server_reconnectInitialDelay} seconds");
                        BouncerMsg("*** Connection to server timed out.... Reconnecting in $config{server_reconnectInitialDelay} seconds");
                      
                        $server_nextConnectionAttempt = time() + $config{server_reconnectInitialDelay};
                
                        RemoveSocket($server_sock) if ($server_sock);
                }
                
                if ($config{server_chat_refreshdelay} > 0)
                {
                        if ($server_playersOnChat_commandSent)
                        {
                                if (!$server_playersOnChat_receivedLines && $time - $server_playersOnChat_commandSent > 20)
                                {
                                        # Haven't received something that looks like the beginning of a ?chat read out for 20 seconds
                                        $server_playersOnChat_lastUpdate = time();
                                        $server_playersOnChat_commandSent = 0;
                                        $server_playersOnChat_receivedLines = 0;
                                        %server_playersOnChat_buffer = ();
                                }
                        }
                        else
                        {
                                if ($time - $server_playersOnChat_lastUpdate > $config{server_chat_refreshdelay})
                                {
                                        UpdatePlayersOnChat();
                                }
                        }
                }
                
                if (length($socketInfo->{buffer}))
                {
                        while (index($socketInfo->{buffer}, "\n") != -1)
                        {
                                my $line = substr($socketInfo->{buffer}, 0, index($socketInfo->{buffer}, "\n") + 1);
                                $socketInfo->{buffer} = substr($socketInfo->{buffer}, length($line));
                                chomp($line);
                                ServerParseMessage($line) if (length($line));
                        }
                }
        }
}

sub UpdatePendingClients
{
        my $time = time();
        my $sock;
        
        foreach $sock (keys %sockets)
        {
                unless ($sock)
                {
                        delete $sockets{$sock};
                        next;
                }
                
                my $socketInfo = $sockets{$sock};
                $sock = $socketInfo->{sock};
                
                if ($socketInfo->{type} == SOCKETTYPE_CLIENTPENDING)
                {
                        my $requiredPass;
                                                
                              if ($config{listen_password})
                              {
                                      $requiredPass = $config{listen_password};
                              }
                              else
                              {
                                      $requiredPass = $config{password};
                              }
                
                        if ($time > $socketInfo->{connectionEstablished} + $config{client_loginTimeout})
                        {
                                l("Pending Client disconnected because logging in took to long");
                                RemoveSocket($sock);
                        }
                        
                        while (index($socketInfo->{buffer}, "\n") != -1)
                        {
                                my $line = substr($socketInfo->{buffer}, 0, index($socketInfo->{buffer}, "\n") + 1);
                                $socketInfo->{buffer} = substr($socketInfo->{buffer}, length($line));
                                chomp($line);
                                if ($line =~ /^LOGIN:1;(.*?):(.+?):(.+?)$/)
                                {
                                        if (lc($2) eq lc($config{nick}))
                                        {
                                                if ($3 eq $requiredPass)
                                                {
                                                        $sock->send("LOGINOK:$server_nick\n", 0, $socketInfo->{sock_peername});
                                                        $socketInfo->{type} = SOCKETTYPE_CLIENT;
                                                        $client_mode = CLIENTMODE_CHATNET;
                                                        if ($client_sock)
                                                        {
                                                                BouncerMsg("*** You have been disconnected because you logged in a second time. (".$sock->peerhost.")");
                                                                l("Client disconnected because someone logged in a second time");
                                                                RemoveSocket($client_sock);
                                                        }
                                                        
                                                        $client_sock = $sock;
                                                        $socketInfo->{nextPing} = $time + $config{client_noopInterval};
                                                        l("Client succesfully logged in. ($1)");
                                                        
                                                        $client_JACC1_2Fixes = ($1 eq 'JACC 1.2, by CypherJF'); 
                                                        
                                                        ClientSendInitialMessages();
                                                        last; #No need to read the buffer further
                                                }
                                                else
                                                {
                                                        $sock->send("LOGINBAD:Wrong password\n", 0, $socketInfo->{sock_peername});
                                                        l("Pending client disconnected: Wrong password");
                                                        RemoveSocket($sock);
                                                }
                                        }
                                        else
                                        {
                                                $sock->send("LOGINBAD:Wrong nickname\n", 0, $socketInfo->{sock_peername});
                                                l("Pending client disconnected: Wrong nickname");
                                                RemoveSocket($sock);
                                        }
                                }
                                elsif ($line =~ /^NICK\s+:?(.+)$/)
                                {
                                        my $configIrcNick = ircNick($config{nick});
                                        
                                        if ($socketInfo->{irc_pass_valid} || $config{client_ircNickServEmulation})
                                        {
                                                if (lc($1) eq lc($configIrcNick))
                                                {
                                                        $socketInfo->{irc_nick_valid} = 1;
                                                        if ($configIrcNick ne $server_ircNick)
                                                        {
                                                                # Make sure the irc client knows about your latest nickname
                                                                $sock->send(":$1!$server_ircNick\@ChatNetBouncer NICK :$server_ircNick\n", 0, $socketInfo->{sock_peername}); 
                                                        }
                                                        
                                                        if ($config{client_ircNickServEmulation})
                                                        {
                                                                $sock->send(":ChatNetBouncer 001 $server_ircNick :ChatNetBouncer $version by JoWie\n", 0, $socketInfo->{sock_peername});
                                                                $sock->send(":NickServ!NickServ\@services. NOTICE $server_ircNick :This nickname is registered. Please identify via /NICKSERV identify <password>.\n", 0, $socketInfo->{sock_peername});
                                                        }
                                                }
                                                else
                                                {
                                                        $sock->send(":ChatNetBouncer 432 * $1 :Invalid Nickname\n", 0, $socketInfo->{sock_peername});
                                                        RemoveSocket($sock);
                                                }
                                        }
                                        else
                                        {
                                                $sock->send(":ChatNetBouncer 464 * :Password required\n", 0, $socketInfo->{sock_peername});
                                                RemoveSocket($sock);
                                        }
                                }
                                elsif ($line =~ /^PASS\s+:?(.+)$/)
                                {
                                        if ($1 eq $requiredPass)
                                        {
                                                $socketInfo->{irc_pass_valid} = 1;
                                        }
                                        else
                                        {
                                                if ($socketInfo->{irc_nick_valid})
                                                {
                                                        $sock->send(":ChatNetBouncer 464 $server_ircNick :Password incorrect\n", 0, $socketInfo->{sock_peername});
                                                }
                                                else
                                                {
                                                        $sock->send(":ChatNetBouncer 464 * :Password incorrect\n", 0, $socketInfo->{sock_peername});
                                                }
                                                RemoveSocket($sock);
                                        }
                                }
                                elsif ($socketInfo->{irc_nick_valid} && 
                                       $line =~ /^(?:PRIVMSG NickServ :|NS |NICKSERV )identify\s+(.*)/i)
                                {
                                        if ($1 eq $requiredPass)
                                        {
                                                $socketInfo->{irc_pass_valid} = 1;
                                                $sock->send(":NickServ!NickServ\@services. NOTICE $server_ircNick :You are now identified for $server_ircNick.\n", 0, $socketInfo->{sock_peername});
                                        }
                                        else
                                        {
                                                $sock->send(":NickServ!NickServ\@services. NOTICE $server_ircNick :Invalid password.\n", 0, $socketInfo->{sock_peername});
                                        }
                                }
                                
                                if ($socketInfo->{irc_pass_valid} && $socketInfo->{irc_nick_valid})
                                {
                                        unless ($config{client_ircNickServEmulation}) # already sent
                                        {
                                                $sock->send(":ChatNetBouncer 001 $server_ircNick :ChatNetBouncer $version by JoWie\n", 0, $socketInfo->{sock_peername});
                                        }
                                        $socketInfo->{type} = SOCKETTYPE_CLIENT;
                                        $client_mode = CLIENTMODE_IRC;
                                        $client_JACC1_2Fixes = 0;
                                        %client_ircMode_joinedChats = ();
                                        
                                        if ($client_sock)
                                        {
                                                BouncerMsg("*** You have been disconnected because you logged in a second time. (".$sock->peerhost.")");
                                                l("Client disconnected because someone logged in a second time");
                                                RemoveSocket($client_sock); # !!!
                                        }
                                        
                                        $client_sock = $sock;
                                        
                                        $socketInfo->{nextPing} = $time + $config{client_noopInterval};
                                        l("Client succesfully logged in. IRC Mode");
                                        
                                        ClientSendInitialMessages();
                                        
                                        last; # No need to read the buffer further
                                }
                        }
                        
                }
        }
}

sub UpdatePlayersOnChat
{
        my $time = time();
        ToServer("SEND:CMD:?chat");    # Send ?chat command,
        ToServer("SEND:PRIV:$server_nick:PING:$time"); # Immediately after that send a PRIV to yourself to end the ?chat response
        $sockets{$server_sock}->{nextPing} = $time + $config{server_pingInterval};
        
        %server_playersOnChat_buffer = ();
        $server_playersOnChat_commandSent = $time;
        $server_playersOnChat_receivedLines = 0;
}

sub UpdatePlayersOnChat_raw
{
        my ($rawMsg) = @_;
        
        return 0 unless ($server_playersOnChat_commandSent);
        
        my $time = time();
        my ($chat, $nick, @foundNicks, $a, $nicks);
        
        if ($rawMsg =~ /^MSG:(?:ARENA|CMD):([^:]+):\s(.*)\s*$/i)
        {
                $chat = $1;
                $nicks = $2;
                
                %server_playersOnChat_buffer = () # If we havent received any ?chat read out before, make sure the buffer is empty 
                        unless ($server_playersOnChat_receivedLines);
                $server_playersOnChat_receivedLines++;
                
                if (length($chat) && length($nicks))
                {
                        @foundNicks = split(',', $nicks);
                        
                        foreach $nick (@foundNicks)
                        {
                                next unless (length($nicks));
                                
                                $server_playersOnChat_buffer{lc($chat)}{lc($nick)} = $nick;
                        }
                }
                
                return 1; # returning true means the arena message is part of the chat read out
        
        }
        else
        {
                # Message not part of ?chat readout
                
                if ($server_playersOnChat_receivedLines)
                {
                        # Reached the end
                        
                        my %old_playersOnChat = %server_playersOnChat;
                        %server_playersOnChat = %server_playersOnChat_buffer;
                        %server_playersOnChat_buffer = ();
                        $server_playersOnChat_commandSent = 0;
                        $server_playersOnChat_receivedLines = 0;
                        $server_playersOnChat_lastUpdate = time();
                        
                        for ($a = 0; $a < scalar(@server_chats); $a++)
                        {
                                $chat = $server_chats[$a];
                                next unless ($client_ircMode_joinedChats{$chat});
                                
                                foreach my $nicklc (keys %{$server_playersOnChat{$chat}})
                                {
                                        next if defined $old_playersOnChat{$chat}{$nicklc} or $nicklc eq $server_ircNicklc;
                                        $nick = ircNick($server_playersOnChat{$chat}{$nicklc});
                                        ToClient(":$nick!CHAT\@ChatNetBouncer JOIN #$chat" , 1);
                                }
                                
                                foreach my $nicklc (keys %{$old_playersOnChat{$chat}})
                                {
                                        next if defined $server_playersOnChat{$chat}{$nicklc} or $nicklc eq $server_ircNicklc;
                                        $nick = ircNick($old_playersOnChat{$chat}{$nicklc});
                                        ToClient(":$nick!CHAT\@ChatNetBouncer PART #$chat" , 1);
                                }
                        }
                        
                }
        }
        return 0;
}

sub JoinIrcChannel
{
        ToClient(":$server_ircNick!$server_ircNick\@ChatNetBouncer JOIN :&ChatNet", 1);
        ToClient(":ChatNetBouncer 332 $server_ircNick &ChatNet :ChatNetBouncer", 1);
        
        SendNamesToIrcClient();
        $client_ircMode_isInChannel = 1;
}

sub JoinIrcChatChannel
{
        my ($channel) = @_;
        $channel = lc($channel);
        $client_ircMode_joinedChats{$channel} = 1;
        ToClient(":$server_ircNick!$server_ircNick\@ChatNetBouncer JOIN :#$channel", 1);
        ToClient(":ChatNetBouncer 332 $server_ircNick #$channel :$channel", 1);        
        SendChatNames($channel);
}

sub SendNamesToIrcClient
{
        my $nicks = ''; #$server_ircNick;
        
        
        foreach (keys %server_players)
        {
                if ($_ eq $server_nicklc)
                {
                        $nicks .= ' +' . $server_players{$_}{irc_nick};
                }
                else
                {
                        $nicks .= ' ' . $server_players{$_}{irc_nick};
                }
        }
        
        local($Text::Wrap::columns) = 200;
        local($Text::Wrap::huge) = 'overflow';
        local($Text::Wrap::separator) = "\n";
        
        my $wrapped = Text::Wrap::wrap(
                ":ChatNetBouncer 353 $server_ircNick = &ChatNet :", 
                ":ChatNetBouncer 353 $server_ircNick = &ChatNet :", 
                Trim($nicks)
        );
        chomp($wrapped);
        
        ToClient($wrapped, 1) if ($wrapped);
        ToClient(":ChatNetBouncer 366 $server_ircNick &ChatNet :End of /NAMES list", 1);
}

sub SendChatNames
{
        my ($chat) = @_;
        
        my $nicks = '';
        
        local($Text::Wrap::columns) = 200;
        local($Text::Wrap::huge) = 'overflow';
        local($Text::Wrap::separator) = "\n";
        
        my $foundSelf = 0;
        foreach (keys %{$server_playersOnChat{$chat}})
        {
                if ($_ eq $server_nicklc)
                {
                        $nicks .= ' +' . $server_ircNick;
                        $foundSelf = 1;
                }
                else
                {
                        $nicks .= ' ' . ircNick($server_playersOnChat{$chat}{$_});
                }
        }
        
        $nicks .= ' +' . $server_ircNick unless $foundSelf;
        
        my $wrapped = Text::Wrap::wrap(
                ":ChatNetBouncer 353 $server_ircNick = #$chat :", 
                ":ChatNetBouncer 353 $server_ircNick = #$chat :", 
                Trim($nicks)
        );
        chomp($wrapped);
        
        ToClient($wrapped, 1) if ($wrapped);
        ToClient(":ChatNetBouncer 366 $server_ircNick #$chat :End of /NAMES list", 1);
}

sub ClientSendInitialMessages
{
        if ($client_mode == CLIENTMODE_IRC)
        {
                JoinIrcChannel();
                if ($config{client_autoJoinChats})
                {
                        for ($a = 0; $a < scalar(@server_chats); $a++)
                        {
                                JoinIrcChatChannel($server_chats[$a]);
                        }
                }

        }
        

        # Client just connected
        BouncerMsg("Welcome, send ?$config{client_bouncerCommand} for bouncer specific messages. Currently ".
                ($server_sock ? "connected to ".$server_sock->peerhost() .":".$server_sock->peerport() . " in arena $server_arena. Your nickname is $server_nick" : "not connected")
        );
        
        if (@awaylogmessages)
        {
                BouncerMsg("You have ".scalar(@awaylogmessages) ." messages. use ?$config{client_bouncerCommand} messages to view them");
        }
        
        if ($client_mode == CLIENTMODE_CHATNET)
        {
                ToClient("INARENA:$server_arena:$server_freq", 1);
                
                my $p;
                foreach $p (keys %server_players)
                {
                        ToClient("PLAYER:$server_players{$p}{nick}:$server_players{$p}{ship}:$server_players{$p}{freq}", 1);
                }
        }
        
}

sub ClientParseMessageChatNet
{
        my ($line) = @_;
        
        my ($type, $remaining) = split(':', $line, 2);
        my ($subtype, $freq, $squad, $nick, $firstChar);
        $type = uc($type);
        my $time = time();
        
        my $forwardMessage = 1;
        
        if ($type eq 'GO')
        {
                $forwardMessage = 0 if ($config{client_ignoreGo});
        }
        elsif($type eq 'CHANGEFREQ')
        {
        }
        elsif ($type eq 'NOOP')
        {
                $forwardMessage = 0;
        }
        elsif ($type eq 'SEND')
        {
                ($subtype, $remaining) = split(':', $remaining, 2);
                $subtype = uc($subtype);
                $firstChar = substr($remaining, 0, 1);
                
                local($Text::Wrap::columns) = ASSS_MAXMESSAGELENGTH-1;
                local($Text::Wrap::huge) = 'wrap';
                local($Text::Wrap::separator) = "\r\n";
                
                if ($subtype eq 'PUB' || $subtype eq 'PUBM' || $subtype eq 'CMD')
                {
                        if ($remaining =~ /^\s*\?\Q$config{client_bouncerCommand}\E(?:\s+(.+))?$/i)
                        {
                                if (defined($1))
                                {
                                        ParseClientBncMessage($1);
                                }
                                else
                                {
                                        ParseClientBncMessage('');
                                }
                                $forwardMessage = 0;
                        }
                        elsif ($remaining =~ /^\?chat(?:=|\s+)(.+)$/i)
                        {
                                @server_chats = split(/,/, lc($1));
                                @server_chatsR{@server_chats} = (0..$#server_chats);
                        }
                        else
                        {
                                $client_lastActivity = $time;
                                unless ($config{client_allowCommands})
                                {
                                        if ($subtype eq 'CMD')
                                        {
                                                $forwardMessage = 0;
                                        }
                                        else
                                        {
                                                if ($firstChar eq '?' || $firstChar eq '*')
                                                {
                                                        $remaining = ' ' . $remaining;
                                                        $firstChar = ' '; # the wrap should now reconstruct the line
                                                }
                                        }
                                }
                                
                                $line = Text::Wrap::wrap("SEND:$subtype:", "SEND:$subtype: ", $remaining)
                                        if ($subtype ne 'CMD' && $firstChar ne '?' && $firstChar ne '*');
                        }
                }
                elsif ($subtype eq 'PRIV' || $subtype eq 'PRIVCMD')
                {
                        ($nick, $remaining) = split(':', $remaining, 2);
                        $firstChar = substr($remaining, 0, 1);
                        $client_lastActivity = $time if (lc($nick) ne lc($server_nick)); #Do not update if pming yourself, some clients may use this for PINGS, etc
                        
                        my $columns = ASSS_MAXMESSAGELENGTH-1;
                        
                        if (substr($nick, 0, 1) eq '#')
                        {
                                # billing_ssc :recipient:(#squad)>name)>message
                                $columns -= 8 + length($server_nick) + length($squad) + 18;
                        }
                        else
                        {
                                unless($server_players{lc($nick)})
                                {
                                        $columns -= 5 + length($server_nick) + length($nick);
                                }
                        }
                        
                        unless ($config{client_allowCommands})
                        {
                                if ($subtype eq 'PRIVCMD')
                                {
                                        $forwardMessage = 0;
                                }
                                else
                                {
                                        if ($firstChar eq '?' || $firstChar eq '*')
                                        {
                                                $remaining = ' ' . $remaining;
                                                $firstChar = ' '; # the wrap should now reconstruct the line
                                        }
                                }
                        }
                        
                        local($Text::Wrap::columns) = $columns;
                        $line = Text::Wrap::wrap("SEND:$subtype:$nick:", "SEND:$subtype:$nick: ", $remaining)
                                        if ($subtype ne 'PRIVCMD' && $firstChar ne '?' && $firstChar ne '*');
                }
                elsif ($subtype eq 'FREQ')
                {
                        ($freq, $remaining) = split(':', $remaining, 2);
                        $firstChar = substr($remaining, 0, 1);
                        $client_lastActivity = $time;
                        
                        unless ($config{client_allowCommands})
                        {
                                if ($firstChar eq '?' || $firstChar eq '*')
                                {
                                        $remaining = ' ' . $remaining;
                                        $firstChar = ' '; # the wrap should now reconstruct the line
                                }
                        }
                        
                        $line = Text::Wrap::wrap("SEND:$subtype:$freq:", "SEND:$subtype:$freq: ", $remaining)
                                        if ($firstChar ne '?' && $firstChar ne '*');
                }
                elsif ($client_JACC1_2Fixes && $subtype eq 'TEAM') # This fix allows you to use // and ' in JACC 1.2
                {
                        ($freq, $remaining) = split(':', $remaining, 2);
                        $client_lastActivity = $time;
                        
                        $forwardMessage = 0;
                        
                        ClientParseMessageChatNet("SEND:FREQ:$server_freq:$remaining");
                        return;
                } 
                elsif ($subtype eq 'CHAT')
                {
                        $client_lastActivity = $time;
                        
                        local($Text::Wrap::columns) = ASSS_MAXMESSAGELENGTH - length($server_nick) - 3;
                        if ($remaining =~ /^(\d+);(.*)$/)
                        {
                                $remaining = $2;
                                $firstChar = substr($remaining, 0, 1);
                                unless ($config{client_allowCommands})
                                {
                                        if ($firstChar eq '?' || $firstChar eq '*')
                                        {
                                                $remaining = ' ' . $remaining;
                                                $firstChar = ' ';
                                        }
                                }
                                $line = Text::Wrap::wrap("SEND:$subtype:$1;", "SEND:$subtype:$1; ", $remaining);
                        }
                        else
                        {
                                $firstChar = substr($remaining, 0, 1);
                                unless ($config{client_allowCommands})
                                {
                                        if ($firstChar eq '?' || $firstChar eq '*')
                                        {
                                                $remaining = ' ' . $remaining;
                                                $firstChar = ' ';
                                        }
                                }
                                $line = Text::Wrap::wrap("SEND:$subtype:", "SEND:$subtype: ", $remaining);
                        }
                }
                elsif ($subtype eq 'MOD')
                {
                        $client_lastActivity = $time;
                        local($Text::Wrap::columns) = ASSS_MAXMESSAGELENGTH - length($server_nick) - 4;
                        
                        $firstChar = substr($remaining, 0, 1);
                        unless ($config{client_allowCommands})
                        {
                                if ($firstChar eq '?' || $firstChar eq '*')
                                {
                                        $remaining = ' ' . $remaining;
                                        $firstChar = ' ';
                                }
                        }
                        $line = Text::Wrap::wrap("SEND:$subtype:", "SEND:$subtype: ", $remaining);
                }
                elsif ($subtype eq 'SQUAD') # This does not even work
                {
                        ($squad, $remaining) = split(':', $remaining, 2);
                        # billing_ssc :recipient:(#squad)>name)>message
                        local($Text::Wrap::columns) = ASSS_MAXMESSAGELENGTH -  (8 + length($server_nick) + length($squad) + 18) - 1; # 18 = maximum player name length
                        
                        $firstChar = substr($remaining, 0, 1);
                        $client_lastActivity = $time;
                        unless ($config{client_allowCommands})
                        {
                                if ($firstChar eq '?' || $firstChar eq '*')
                                {
                                        $remaining = ' ' . $remaining;
                                        $firstChar = ' ';
                                }
                        }
                        $line = Text::Wrap::wrap("SEND:$subtype:", "SEND:$subtype: ", $remaining);
                }
        }
        
        if ($forwardMessage)
        {
                ToServer($line);
        }
}

sub ServerParseMessage
{
        my ($line) = @_;
        l("S << $line") if ($config{server_logRaw} && !($line =~ /^MSG:PRIV:([^:]+):PING:/ && $1 eq $server_nick));

        my ($type, $remaining) = split(':', $line, 2);
        my ($subtype, $nick, $nicklc, $chnum, $squad, $arena, $freq, $ship, $killer, $killed, $bounty, $flagscarried);
        $type = uc($type);
        my $time = time();
        
        my $forwardMessage = 1;
        $forwardMessage = !UpdatePlayersOnChat_raw($line);
        
        if ($type eq 'LOGINOK')
        {
                my $oldnick = $server_nick;
                
                %usedIrcNicks = (); # reset used irc nicks
                
                $server_nick = $remaining;
                $server_nicklc = lc($server_nick);
                $server_ircNick = createIrcNick($server_nick);
                $server_ircNicklc = lc($server_ircNick);
                
                $server_billerDown = substr($server_nick, 0, 1) eq '^';
                
                %server_players = (); # reset player list
                $server_players{$server_nicklc}{nick} = $server_nick;
                $server_players{$server_nicklc}{irc_nick} = $server_ircNick; #bug?
                $server_players{$server_nicklc}{ship} = 8;
                $server_players{$server_nicklc}{freq} = -1;
                $server_players{$server_nicklc}{enter_count} = 1;
                
                $server_playersOnChat_commandSent = 0;
                $server_playersOnChat_receivedLines = 0;
                $server_playersOnChat_lastUpdate = $time;
                %server_playersOnChat_buffer = ();
                
                
                $forwardMessage = 0;
                l("Login OK, nickname: $server_nick");
                BouncerMsg("*** Succesfully Logged in. Nickname: $server_nick");
                
                ToServer("GO:$config{server_arena}");
                @server_chats = ();
                %server_chatsR = ();
        }
        elsif ($type eq 'LOGINBAD')
        {
                l("Login failed: $remaining");
                BouncerMsg("*** Login failed: $remaining");
                
                if ($remaining eq 'Wrong password')
                {
                        die 'Unable to login to server; Incorrect password';
                }
                
                $forwardMessage = 0;
        }
        elsif ($type eq 'INARENA')
        {
                ($arena, $freq) = split(':', $remaining, 2);
                $server_arena = $arena;
                $server_arena = '' unless(defined($server_arena));
                $server_freq = $freq;
                %server_players = (); # reset player list
                
                $server_players{$server_nicklc}{nick} = $server_nick;
                $server_players{$server_nicklc}{irc_nick} = $server_ircNick;
                $server_players{$server_nicklc}{ship} = $freq;
                $server_players{$server_nicklc}{freq} = -1;
                $server_players{$server_nicklc}{enter_count} = 1;
                
                %usedIrcNicks = ($server_ircNicklc => $server_nick); # reset used irc nicks
                
                @server_chats = split(/,/, lc($config{chat}));
                @server_chatsR{@server_chats} = (0..$#server_chats);
                ToServer("SEND:CMD:?chat=$config{chat}");
                ToServer("SEND:CMD:?obscene");
        }
        elsif ($type eq 'PLAYER' || $type eq 'ENTERING')
        {
                ($nick, $ship, $freq) = split(':', $remaining, 3);
                $nicklc = lc($nick);
                $server_players{$nicklc}{nick} = $nick;
                $server_players{$nicklc}{irc_nick} = createIrcNick($nick) unless defined($server_players{$nicklc}{irc_nick}); # {irc_nick}
                $server_players{$nicklc}{ship} = $ship;
                $server_players{$nicklc}{freq} = $freq;
                $server_players{$nicklc}{enter_count} = 0 unless (defined $server_players{$nicklc}{enter_count});
                $server_players{$nicklc}{enter_count}++;
                
                if ($server_players{$nicklc}{enter_count} > 1) # I havent seen any chatnet client yet that handles duplicate names correctly, therefor do not send it
                {
                        $forwardMessage = 0;
                }
                
                #if ($server_billerDown && $type eq 'ENTERING' && substr($nick, 0, 1) ne '^') # Biller is apparently back, reconnect
                #{
                #        BouncerMsg("Reconnecting... (server reconnected to biller; A player entered without a ^ infront of his name)");
                #        RemoveSocket($server_sock);
                #}
        }
        elsif ($type eq 'LEAVING')
        {
                $nick = $remaining;
                $nicklc = lc($nick);
                $server_players{$nicklc}{enter_count}--;
                
                $forwardMessage = 0;
                
                if ($server_players{$nicklc}{enter_count} < 1) # No players left with this name
                {
                        # Send the client the leave before the user is actually removed; this is need for ChatNetToIrc
                        ToClient($line,0, 1); # 3rd arg = comes directly from server
                        
                        freeIrcNick($server_players{$nicklc}{irc_nick});
                        delete $server_players{$nicklc};
                }
        }
        elsif ($type eq 'SHIPFREQCHANGE')
        {
                if ($remaining =~ /^(.+?):(\d+):(\d+)$/) # SHIPFREQCHANGE:nick:ship:freq nick may contain :
                {
                        ($nick, $ship, $freq) = ($1, $2, $3);
                        $nicklc = lc($nick);
                        
                        if ($nicklc eq $server_nicklc)
                        {
                                $server_freq = $freq;
                        }
                        else
                        {
                                $server_players{$nicklc}{nick} = $nick;
                                $server_players{$nicklc}{ship} = $ship;
                                $server_players{$nicklc}{freq} = $freq;
                        }
                }
        }
        elsif ($type eq 'KILL')
        {
                ($killer, $killed, $bounty, $flagscarried) = split(':', $remaining, 4);
        }
        elsif ($type eq 'NOOP')
        {
                $forwardMessage = 0;
        }
        elsif ($type eq 'MSG')
        {
                ($subtype, $remaining) = split(':', $remaining, 2);
                $subtype = uc($subtype);
                
                if ($subtype eq 'ARENA')
                {
                        if ($remaining eq 'Obscene filter ON')
                        {
                                ToServer("SEND:CMD:?obscene");
                        }
                        elsif ($remaining eq 'Obscene filter OFF')
                        {
                                $forwardMessage = 0;
                        }
                        elsif ($remaining eq 'NOTICE: Powerball game will be automatically reset if no goals are scored in the next 60 seconds.' || 
                               $remaining eq 'Powerball game reset.')
                        {
                                $forwardMessage = 0;
                        }
                        elsif ($remaining eq 'Notice: Connection to user database server restored. Log in again for full functionality')
                        {
                                BouncerMsg("Reconnecting... (server reconnected to biller)");
                                RemoveSocket($server_sock); 
                        }
                        # Hyperspace specific:
                        elsif ($remaining =~ /^Player (.+?) gave you \$(\d+)\.(?: Message: \"(.+)\")?$/) #"))
                        {
                                $nicklc = lc($1);
                                # amount = $2
                                # message = $3
                                if (exists $ignoreList{$nicklc})
                                {
                                        $forwardMessage = 0;
                                }
                        }
                        elsif ($remaining =~ /^Player (.+?) has (?:\$\d+ in their account(?: and \d+ experience)?|\d+ experience)\.$/)
                        {
                                # Player JoWie has $10400 in their account.
                                # Player JoWie has $10000 in their account and 0 experience.
                                # Player JoWie has 0 experience.
                                $nicklc = lc($1);
                                if (exists $ignoreList{$nicklc})
                                {
                                        $forwardMessage = 0;
                                }
                        }
                        else
                        {
                                if ($client_ircMode_ison_findsLeft > 0) # stuff below is not 100% perfect due to the way biller outputs things and the lack of spaces in irc nicks
                                {
                                        if ($remaining eq 'Unknown user.')
                                        {
                                                $client_ircMode_ison_findsLeft--;
                                                $forwardMessage = 0;
                                        }
                                        elsif ($remaining =~ /^Not online, last seen/)
                                        {
                                                $client_ircMode_ison_findsLeft--;
                                                $forwardMessage = 0;
                                        }
                                        elsif ($remaining =~ /^(.*) is in arena (\S*)\.$/)
                                        {
                                                $client_ircMode_ison_findsLeft--;
                                                push(@client_ircMode_ison, getIrcNick($1, 1, 1));
                                                $forwardMessage = 0;
                                        }
                                        elsif ($remaining =~ /^(.*) is in a private arena\.$/)
                                        {
                                                $client_ircMode_ison_findsLeft--;
                                                push(@client_ircMode_ison, getIrcNick($1, 1, 1));
                                                $forwardMessage = 0;
                                        }
                                        elsif ($remaining =~ /^(.*) is in .*?$/) # no way to make this perfect, but i doubt there will be a zone called "is in"
                                        {
                                                $client_ircMode_ison_findsLeft--;
                                                push(@client_ircMode_ison, getIrcNick($1, 1, 1));
                                                $forwardMessage = 0;
                                        }
                                        
                                        
                                        if ($client_ircMode_ison_findsLeft <= 0)
                                        {
                                                ToClient(":ChatNetBouncer 303 $server_ircNick :" . join(' ', @client_ircMode_ison), 1);
                                                @client_ircMode_ison = ();
                                                $client_ircMode_ison_findsLeft = 0;
                                                $client_ircMode_ison_startedAt = 0; 
                                        }
                                }
                        }
                }
                elsif ($subtype eq 'CMD')
                {
                }
                elsif ($subtype eq 'PUB' || $subtype eq 'PUBM')
                {
                        ($nick, $remaining) = split(':', $remaining, 2);
                        $nicklc = lc($nick);
                        if (exists $ignoreList{$nicklc})
                        {
                                $forwardMessage = 0;
                        }
                }
                elsif ($subtype eq 'PRIV')
                {
                        ($nick, $remaining) = split(':', $remaining, 2);
                        $nicklc = lc($nick);
                        
                        if ($nicklc eq $server_nicklc)
                        {
                                if ($remaining =~ /^PING:(.*)$/)
                                {
                                        $forwardMessage = 0;
                                }
                        }
                        else
                        {
                                if (exists $ignoreList{$nicklc})
                                {
                                        $forwardMessage = 0;
                                }
                                else
                                {
                                        if (($config{client_awayAfterDisconnect} && !$client_sock) || 
                                           $client_isAway || 
                                           ($client_mode == CLIENTMODE_IRC && !$client_ircMode_isInChannel) ||
                                           ($config{client_awayAfterInactivity} && $time > $client_lastActivity + $config{client_awayAfterInactivity}))
                                        {
                                                my $awayIgnored = 0;
                                                foreach (@{$config{client_awayIgnore}})
                                                {
                                                        if ($_ eq $nicklc)
                                                        {
                                                                $awayIgnored = 1;
                                                                last;
                                                        }
                                                }
                                        
                                                if (!$awayIgnored)
                                                {
                                                        if (length($client_awayMessage))
                                                        {
                                                                if (!$lastMessageLogged{$nicklc} || $time > $lastMessageLogged{$nicklc} + 15) 
                                                                {
                                                                        ToServer("SEND:PRIV:$nick:$client_awayMessage");
                                                                        ToClient("MSG:ARENA:$nick> $client_awayMessage");
                                                                }       
                                                        }
                                                        $lastMessageLogged{$nicklc} = $time;
                                                        ClientLogMessage($nick, $remaining);
                                                }
                                        }
                                }
                        }
                }
                elsif ($subtype eq 'FREQ')
                {
                        ($nick, $remaining) = split(':', $remaining, 2);
                        $nicklc = lc($nick);
                        if (exists $ignoreList{$nicklc})
                        {
                                $forwardMessage = 0;
                        }
                }
                elsif ($subtype eq 'CHAT')
                {
                        ($chnum, $remaining) = split(':', $remaining, 2);
                        ($nick, $remaining) = split('> ', $remaining, 2);
                        
                        $nicklc = lc($nick);
                        if (exists $ignoreList{$nicklc})
                        {
                                $forwardMessage = 0;
                        }
                        
                        if ($client_JACC1_2Fixes && length($nick) < 5) # odd bug in JACC....
                        {
                                $nick .= ' ' x (5-length($nick));
                                $line = "MSG:CHAT:$chnum:$nick> $remaining";
                        }        
                }
                elsif ($subtype eq 'MOD')
                {
                        ($nick, $remaining) = split(':', $remaining, 2);
                        
                        $nicklc = lc($nick);
                        if (exists $ignoreList{$nicklc})
                        {
                                $forwardMessage = 0;
                        }
                }
                elsif ($subtype eq 'SYSOP')
                {
                }
                elsif ($subtype eq 'SQUAD')
                {
                        ($squad, $nick, $remaining) = split(':', $remaining, 3);
                        $nicklc = lc($nick);
                        if (exists $ignoreList{$nicklc})
                        {
                                $forwardMessage = 0;
                        }
                }
        }
        
        if ($forwardMessage)
        {
                ToClient($line,0, 1); # 3rd arg = comes directly from server 
        }
}

sub ToServer
{
        my ($msg, $logmsg) = @_;
        
        $logmsg = $msg unless ($logmsg);
        
        if ($server_sock)
        {
                if ($server_sock->send("$msg\r\n", 0, $sockets{$server_sock}->{sock_peername}) < 0)
                {
                        l("Socket to server closed. (write)");
                        BouncerMsg("*** Connection to server lost (socket closed)");
                        RemoveSocket($server_sock);
                }
                else
                {
                        l("S >> $logmsg") if ($config{server_logRaw} && !($logmsg =~ /^SEND:PRIV:([^:]+):PING:/ && $1 eq $server_nick));
                }
        }
}

sub ToClient
{
        # If $noModeParsing is 1, do not attempt to translate chatnet to irc (if communicating with an irc client)
        my ($msg, $noModeParsing, $fromServer) = @_;
        my $msgEncoded;
        
        if ($client_mode == CLIENTMODE_IRC && !$noModeParsing)
        {
                ChatNetToIrc($msg, $fromServer);
                return;
        }
        
        if ($client_sock)
        {
        
                unless (defined $sockets{$client_sock}->{sock_peername})
                {
                        print Dumper(\%sockets);
                        print "client_sock = $client_sock\n";
                        print "server_sock = $server_sock\n";
                        print "listen_sock = $listen_sock\n";
                        die;
                }
                
                $msgEncoded = $msg;
                if ($client_mode == CLIENTMODE_IRC && $config{client_ircUTF8})
                {
                        Encode::from_to($msgEncoded, "cp1252", "utf8");
                }
                
                if ($client_sock->send("$msgEncoded\r\n", 0, $sockets{$client_sock}->{sock_peername}) < 0) #TODO: "send: Cannot determine peer address"
                {
                        l("Socket to client closed. (write)");
                        RemoveSocket($client_sock);
                }
                else
                {
                        l("C >> $msg") if ($config{client_logRaw});
                }
        }
}

sub IrcColor
{
        my ($foreground, $background) = @_;
        
        return '' unless ($config{client_ircColors});
        
#         0 white
#         1 black
#         2 blue     (navy)
#         3 green
#         4 red
#         5 brown    (maroon)
#         6 purple
#         7 orange   (olive)
#         8 yellow
#         9 lt.green (lime)
#         10 teal    (a kinda green/blue cyan)
#         11 lt.cyan (cyan ?) (aqua)
#         12 lt.blue (royal)
#         13 pink    (light purple) (fuchsia)
#         14 grey
#         15 lt.grey (silver)

        $foreground = '0' . $foreground if (length($foreground) < 2);

        if (defined($background))
        {
                $background = '0' . $background if (length($background) < 2);
                return "\003${foreground},${background}";                
        }

        return "\003${foreground}";
}

sub IrcColorEnd
{
        return "\003";
}

sub stripIrcColor
{
        my ($line) = @_;
        $line =~ s/\003((\d\d?)(,(\d\d?))?)?//g; # Color
        $line =~ s/(\037|\002|\026|\017)//g;     # Underline, bold, reverse, plain
        return $line;
}

sub ChatNetToIrc
{
        my ($line, $fromServer) = @_;

        my ($type, $remaining) = split(':', $line, 2);
        my ($subtype, $nick, $nicklc, $chnum, $chname, $squad, $arena, $freq, $ship, $killer, $killed, $bounty, $flagscarried, $sent);
        $type = uc($type);
        my $time = time();

        if ($client_ircMode_gatheringPlayers && $fromServer && $type ne 'PLAYER')
        {
                SendNamesToIrcClient();
                $client_ircMode_gatheringPlayers = 0;
        }        
       
        if ($type eq 'LOGINOK')
        {
                ToClient(":$1!$server_ircNick\@ChatNetBouncer NICK :$server_ircNick\n", 1);
        }
        elsif ($type eq 'NOOP')
        {
                ToClient("PING :ChatNetBouncer", 1);
        }
        elsif ($type eq 'INARENA')
        {
                $client_ircMode_gatheringPlayers = 1;
                print "Now in arena $server_arena\n";
                BouncerMsg("*** Now in arena $server_arena");
                return;
        }
        
        return unless ($client_ircMode_isInChannel); # Messages below here only get sent if you are in the channel
        
        if ($type eq 'ENTERING')
        {
                ($nick, $ship, $freq) = split(':', $remaining, 3);
                $nick = getIrcNick($nick);
                ToClient(":$nick!$type\@ChatNetBouncer JOIN :&ChatNet", 1);
        }
        elsif ($type eq 'LEAVING')
        {
                ($nick, $ship, $freq) = split(':', $remaining, 3);
                $nick = getIrcNick($nick);
                ToClient(":$nick!$type\@ChatNetBouncer PART &ChatNet :", 1);
        }

        elsif ($type eq 'MSG')
        {
                ($subtype, $remaining) = split(':', $remaining, 2);
                $subtype = uc($subtype);
                
                if ($subtype eq 'ARENA')
                {
                        ToClient(":${ircSpace}!$subtype\@ChatNetBouncer PRIVMSG &ChatNet :".IrcColor(3).$remaining, 1);
                }
                elsif ($subtype eq 'CMD')
                {
                        ToClient(":${ircSpace}!$subtype\@ChatNetBouncer PRIVMSG &ChatNet :".IrcColor(3).$remaining, 1);
                }
                elsif ($subtype eq 'PUB' || $subtype eq 'PUBM')
                {
                        ($nick, $remaining) = split(':', $remaining, 2);
                        $nick = getIrcNick($nick);
                        ToClient(":$nick!$subtype\@ChatNetBouncer PRIVMSG &ChatNet :".$remaining, 1);
                }
                elsif ($subtype eq 'PRIV')
                {
                        ($nick, $remaining) = split(':', $remaining, 2);
                        $nick = getIrcNick($nick);
                        
                        if ($remaining =~ /^NOTICE:(.*)/)
                        {
                                ToClient(":$nick!$subtype\@ChatNetBouncer NOTICE $server_ircNick :".$1, 1);
                        }
                        else
                        {
                                ToClient(":$nick!$subtype\@ChatNetBouncer PRIVMSG $server_ircNick :".$remaining, 1);
                        }
                }
                elsif ($subtype eq 'FREQ')
                {
                        ($nick, $remaining) = split(':', $remaining, 2);
                        $nick = getIrcNick($nick);
                        ToClient(":F:$nick!$subtype\@ChatNetBouncer PRIVMSG &ChatNet :".IrcColor(7).$remaining, 1);
                }
                elsif ($subtype eq 'CHAT')
                {
                        ($chnum, $remaining) = split(':', $remaining, 2);
                        ($nick, $remaining) = split('> ', $remaining, 2);
                        
                        $nick = getIrcNick($nick);
                        
                        $sent = 0;
                        if (defined($server_chats[$chnum-1]))
                        {
                                $chname = $server_chats[$chnum-1];
                                if ($client_ircMode_joinedChats{$chname})
                                {
                                        ToClient(":$nick!$subtype\@ChatNetBouncer PRIVMSG #$chname :".$remaining, 1);
                                        $sent = 1;
                                }
                        }
                        
                        if ($config{client_doubleJoinedChatsMsgs} || !$sent)
                        {
                                ToClient(":C:$chnum:$nick!$subtype\@ChatNetBouncer PRIVMSG &ChatNet :".IrcColor(4).$remaining, 1);
                        }
                }
                elsif ($subtype eq 'MOD')
                {
                        ($nick, $remaining) = split(':', $remaining, 2);
                        
                        $nick = getIrcNick($nick);
                        ToClient(":M:$nick!$subtype\@ChatNetBouncer PRIVMSG &ChatNet :".IrcColor(4).$remaining, 1);
                }
                elsif ($subtype eq 'SYSOP')
                {
                        ToClient(":${ircSpace}!$subtype\@ChatNetBouncer PRIVMSG &ChatNet :".IrcColor(6).$remaining, 1);
                }
                elsif ($subtype eq 'SQUAD')
                {
                        ($squad, $nick, $remaining) = split(':', $remaining, 3);
                        $nick = getIrcNick($nick);
                        $squad = getIrcNick($squad, 1); # Dont use the player table
                        ToClient(":S:($squad)($nick)!$subtype\@ChatNetBouncer PRIVMSG &ChatNet :".IrcColor(3).$remaining, 1);
                }
        } 
}

sub ClientParseMessageIRC
{
        my ($line) = @_; # ChatNetBouncer PRIVMSG Something :test
        my ($target, $targetlc, $chatnetNick, $channelPrefix, $channel, $channelKey, $firstChar, $secondChar); 
        my ($a, $b, @nicks, @ison, @noton, @channels, $ln, $mode, $mask, $reason);
        my ($type, $remaining) = split(' ', $line, 2);
        $type = uc($type);

        
        if ($type eq 'PING') 
        {
                #PING :ChatNetBouncer
                
                # No need to forward to the server
                ToClient("ChatNetBouncer PONG $remaining", 1);
        }
        elsif ($type eq 'PONG')
        {
        }
        elsif ($type eq 'PRIVMSG')
        {
                #PRIVMSG Something :test
                ($target, $remaining) = split(' :', $remaining, 2);
                $targetlc = lc($target);
                
                if ($targetlc eq 'nickserv')
                {
                        # Prevent leaking password by identify scripts
                        BouncerMsg("Private message to 'nickserv' has been ignored.");
                        return;
                }
                
                $channelPrefix = substr($targetlc, 0, 1);
                $channel = substr($targetlc, 1);
                
                $remaining = stripIrcColor($remaining);
                
                $firstChar = substr($remaining, 0, 1);
                $secondChar = substr($remaining, 1, 1);
                
                if ($channelPrefix eq '&') # To chat
                {
                        $firstChar = ' ' unless ($config{client_allowTokens});
                        if ($channel eq 'chatnet')
                        {
                                if (($firstChar eq '/' && $secondChar eq '/') || $firstChar eq "'")
                                {
                                        ClientParseMessageChatNet('SEND:FREQ:'.
                                                $server_freq.
                                                ':'. substr($remaining, $firstChar eq "'" ? 1 : 2)  
                                                );
                                }
                                elsif ($firstChar eq ':')
                                {
                                        my $colon = index($remaining, ':', 1);
                                        
                                        if ($colon < 0)
                                        {
                                                ClientParseMessageChatNet('SEND:PUB:'.$remaining);
                                        }
                                        else # :JoWie:abc
                                        {
                                                ClientParseMessageChatNet('SEND:PRIV:'.
                                                        lc(substr($remaining, 1, $colon - 1))
                                                        .':'.substr($remaining, $colon+1));
                                        }
                                }
                                elsif ($firstChar eq ';')
                                {
                                        ClientParseMessageChatNet('SEND:CHAT:'.substr($remaining, 1));             
                                }
                                elsif ($firstChar eq '\\')
                                {
                                        ClientParseMessageChatNet('SEND:MOD:'.substr($remaining, 1));
                                }
                                elsif ($firstChar eq '"' || $firstChar eq '/')
                                {
                                        BouncerMsg("\" and / are not supported in public chat");
                                }
                                else
                                {
                                        ClientParseMessageChatNet('SEND:PUB:'.$remaining);
                                }
                        }
                        else
                        {
                                ToClient(":ChatNetBouncer 403 $server_ircNick $target :That channel does not exist (try &ChatNet)", 1);
                        }
                }
                elsif ($channelPrefix eq '#') # ?chat channel
                {
                        if (defined($server_chatsR{$channel}))
                        {
                                ClientParseMessageChatNet('SEND:CHAT:'.($server_chatsR{$channel}+1).';'.$remaining);
                        }
                        else
                        {
                                ToClient(":ChatNetBouncer 403 $server_ircNick $target :You are not on that ?chat", 1);
                        }
                }
                else # To a player
                {
                        $chatnetNick = getChatnetNick($target);
                        $firstChar = ' ' unless ($config{client_allowTokens});
                        
                        if ($firstChar eq '"')
                        {        
                                if (defined $server_players{$chatnetNick})
                                {
                                        ClientParseMessageChatNet('SEND:FREQ:'.
                                                $server_players{$chatnetNick}{freq}.
                                                ':'.substr($remaining,1));
                                }
                                else
                                {
                                        BouncerMsg("\" is not supported in cross arena/zone private messages");
                                }
                        }
                        elsif ($firstChar eq ':')
                        {
                                my $colon = index($remaining, ':', 1);
                                
                                if ($colon < 0)
                                {
                                        ClientParseMessageChatNet('SEND:PUB:'.$remaining);
                                }
                                else # :JoWie:abc
                                {
                                        ClientParseMessageChatNet('SEND:PRIV:'.
                                                lc(substr($remaining, 1, $colon - 1))
                                                .':'.substr($remaining, $colon+1));
                                }
                        }
                        elsif ($firstChar eq '/' || $firstChar eq '\\' || $firstChar eq "'" || $firstChar eq ';')
                        {
                                BouncerMsg("// '  ; /  and \ are not supported in private messages");
                        }
                        else
                        {
                                ClientParseMessageChatNet('SEND:PRIV:'.$chatnetNick.':'.$remaining);
                        }
                
                }
        }
        elsif ($type eq 'NOTICE')
        {
                ($target, $remaining) = split(' :', $remaining, 2);
                $targetlc = lc($target);
                if ($targetlc ne '&chatnet')
                {
                        $chatnetNick = getChatnetNick($target);
                        ClientParseMessageChatNet('SEND:PRIV:'.$chatnetNick.':NOTICE:'.$remaining);        
                }
        }
        elsif ($type eq 'NAMES')
        {
                $target = $remaining;
                $targetlc = lc($target);
                
                $channelPrefix = substr($targetlc, 0, 1);
                $channel = substr($targetlc, 1);
                
                if ($channelPrefix eq '&')
                {
                        if ($channel eq 'chatnet')
                        {
                                SendNamesToIrcClient();  
                        }
                        else
                        {
                                ToClient(":ChatNetBouncer 403 $server_ircNick $target :That channel does not exist (try &ChatNet)", 1);
                        }
                }
                elsif ($channelPrefix eq '#')
                {        
                        SendChatNames($channel);
                }
                else
                {
                        ToClient(":ChatNetBouncer 403 $server_ircNick $target :That channel does not exist (try &ChatNet)", 1);
                }
        }
        elsif ($type eq 'JOIN')
        {
                @channels = split(',', $remaining);
                foreach $target (@channels)
                {
                        ($target, $channelKey) = split(' ', $target, 2);
                        
                        $channelPrefix = substr($target, 0, 1);
                        $channel = lc(substr($target, 1));
                        
                        if ($channelPrefix eq '&')
                        {
                                if ($channel eq 'chatnet')
                                {
                                        JoinIrcChannel();
                                }
                                else
                                {
                                        ToClient(":ChatNetBouncer 403 $server_ircNick $target :That channel does not exist (try &ChatNet)", 1);
                                }
                        }
                        elsif ($channelPrefix eq '#')
                        {
                                JoinIrcChatChannel($channel);
                        }
                        else
                        {
                                ToClient(":ChatNetBouncer 403 $server_ircNick $target :That channel does not exist (try &ChatNet)", 1);
                        }
                }
        }
        elsif ($type eq 'PART')
        {
                ($target, $remaining) = split(' :', $remaining, 2);
                $targetlc = lc($target);
                #PART #test
                #PART #test :abc
                $channelPrefix = substr($targetlc, 0, 1);
                $channel = substr($targetlc, 1);
                
                if ($channelPrefix eq '&')
                {
                        if ($channel eq 'chatnet')
                        {
                                $client_ircMode_isInChannel = 0;
                                #ToClient("ERROR :Closing Link: ChatNetBouncer (You parted the ChatNet channel)", 1);
                        }
                        else
                        {
                                ToClient(":ChatNetBouncer 403 $server_ircNick $target :That channel does not exist", 1);
                        }
                }
                elsif ($channelPrefix eq '#')
                {
                        delete $client_ircMode_joinedChats{$channel};
                }
                else
                {
                        ToClient(":ChatNetBouncer 403 $server_ircNick $target :That channel does not exist", 1);
                }
        }
        elsif ($type eq 'QUIT')
        {
                ToClient("ERROR :Closing Link: ChatNetBouncer (Quit$remaining)", 1);
                RemoveSocket($client_sock);
        }
        elsif ($type eq 'USERHOST')
        {
                $target = $remaining;
                $chatnetNick = $usedIrcNicks{lc($target)};
                
                if (defined $chatnetNick)
                {
                        $target = $server_players{lc($chatnetNick)}{irc_nick}; # Make sure we reply with the correct capitalisation
                        ToClient("302 $target :$target=$target\@ChatNetBouncer", 1);
                }
                else
                {
                        ToClient("302 $target :", 1); 
                }
        }
        elsif ($type eq 'ISON')
        {
                # ISON name1 name2 name3 name4
                # 303 $target :name2 name4

                
                @ison = ();
                @noton = ();
                @nicks = split(' ', $remaining);
                foreach $target (@nicks)
                {
                        $chatnetNick = getChatnetNick($target);
                        
                        if (exists $server_players{lc($chatnetNick)})
                        {
                                push(@ison, $target);
                        }
                        else
                        {
                                push(@noton, $target);
                        }
                }
                
                if (scalar(@noton)) 
                {
                        $ln = "";
                        $b = 0;
                        
                        for ($a = 0; $a < scalar(@noton); $a++)
                        {
                                if (index($noton[$a], '|') >= 0) # name contains a |
                                {
                                        ToServer("SEND:CMD:?find ".$noton[$a]);
                                }
                                else
                                {
                                        $ln .= "|find ". $noton[$a];
                                        $b++;
                                        
                                        if ($b >= 4)
                                        {
                                                ToServer("SEND:CMD:?$ln");
                                                $b = 0;
                                                $ln = "";
                                        }
                                }
                        }
                        
                        if ($b)
                        {
                                ToServer("SEND:CMD:?$ln");
                        }
                        
                        $client_ircMode_ison_findsLeft += $a;
                        push(@client_ircMode_ison, @ison);
                        $client_ircMode_ison_startedAt = time();
                        
                }
                else # all in this arena, send response immediately
                {
                        #:pratchett.freenode.net 303 WILD_WILD_WEB :bla 
                        ToClient(":ChatNetBouncer 303 $server_ircNick :" . join(' ', @ison), 1); 
                }
        }
        elsif ($type eq uc($config{client_bouncerCommand}))
        {
                if (defined($remaining))
                {
                        ParseClientBncMessage($remaining);
                }
                else
                {
                        ParseClientBncMessage('');
                }
        }
        elsif ($type eq 'MODE')
        {
                ($channel, $mode) = split(' ', $remaining, 2);
                # :weber.freenode.net 324 JoWieBla #jsdom +t
                if (defined($mode))
                {
                        ToClient("477 $server_ircNick $channel :Channel doesn't support modes", 1);
                }
                else
                {
                        ToClient("324 $server_ircNick $channel +tc", 1);
                }
        }
        elsif ($type eq 'WHO')
        {
                $mask = $remaining;
                #ToClient("315 $server_ircNick $mask :End of /WHO list.", 1);
        }
        elsif ($type eq 'AWAY')
        {
                $reason = $remaining;
                chomp($reason);
                $reason =~ s/^:// if ($reason);
                
                if ($reason)
                {
                        $client_isAway = 1;
                        $client_awayMessage = $reason;
                        ToClient("306 $server_ircNick $type :You have been marked as being away", 1);
                }
                else
                {
                        $client_isAway = 0;
                        ToClient("305 $server_ircNick $type :You are no longer marked as being away", 1);
                }
        }
        else
        {
                ToClient("421 $server_ircNick $type :Unknown command", 1);
        }
}


sub ParseClientBncMessage
{
        my ($args) = @_;
        $args = Trim($args);
        my ($command, @lines, %commands, $a);
        ($command, $args) = split(' ', $args, 2);
        $command = defined($command) ? lc($command) : '';
        $args = '' unless defined($args);
        
        %commands = (
                help  => "Args: <command name>\nDisplays help on a command or gives a list of commands if <command name> is not given",
                go    => "Args: <arena name>\nSends you to a different arena, this will persist between reconnects",
                tgo   => "Args: <arena name>\nSends you to a different arena, you will be sent back to default arena after you log out or the connection to the server is lost",
                raw   => "Args: <message>\nSends a raw message to the server",
                reconnect => "Args: none\nReconnects the connection to the server",
                about => "Args: none\nShows version and author of this bouncer",
                away  => "Args: none\nToggles being away on / off. If you are away any messages will be logged with an auto reply. The auto reply can be changed using the command awaymessage",
                awaymessage => "Args: <away message>\nSets you as away and logs any messages sent to you. Auto replies with <away message>, auto reply can be disabled by using ?$config{client_bouncerCommand} away empty",
                messages => "Args:none\nShows your messages",
                clearmessages => "Args:none\nClears your messages",
                ignore => " Args: <player>\nIgnores the specified player",
                unignore => " Args: <player>\nUnignores the specified player",
                ignoreirc => " Args: <irc_nick>\nIgnores the specified player by using its irc nick",
                unignoreirc => " Args: <irc_nick>\nUnignores the specified player by using its irc nick",
                playercount => "Args: none\nShows the number of players in this arena",
                chat => "Args: none\nShows all the players on your chat channels",
        );

        if ($command eq 'help' || $command eq 'man' || $command eq '')
        {       
                if (length($args))
                {
                        if (defined($commands{$args}))
                        {
                                BouncerMsg("Help on ?$config{client_bouncerCommand} $args:");
                                @lines = split("\n", $commands{$args});
                                BouncerMsg('  '.$_) foreach @lines;
                                
                        }
                        else
                        {
                                BouncerMsg("Sorry, I don't know anything about ?$config{client_bouncerCommand} $args");
                        }
                }
                else
                {
                        BouncerMsg("Available commands: ".join(' ', keys %commands));
                        BouncerMsg("Use ?$config{client_bouncerCommand} help <command> for more info.");
                }
        }
        elsif ($command eq 'go')
        {
                $config{server_arena} = $args;
                ToServer("GO:$args");
        }
        elsif ($command eq 'tgo')
        {
                ToServer("GO:$args");
        }
        elsif ($command eq 'raw')
        {
                if (length($args))
                {
                        ToServer($args);
                }
        }
        elsif ($command eq 'about')
        {
                BouncerMsg("ChatNetBouncer $version by JoWie");
        }
        elsif ($command eq 'away')
        {
                $client_isAway = !$client_isAway;
                
                if ($client_isAway)
                {
                        BouncerMsg("You are now set to away. Message: $client_awayMessage");
                }
                else
                {
                        BouncerMsg("You are no longer away");
                }
        }
        elsif ($command eq 'awaymessage')
        {
                $client_awayMessage = (length($args) > 0 ? $args : $config{client_awayMessage});
                if (lc($client_awayMessage) eq 'empty')
                {
                        $client_awayMessage = '';
                }
                BouncerMsg("Away message set to: $client_awayMessage");
        }
        elsif ($command eq 'clearmessages')
        {
                ClearClientLogMessages();
                BouncerMsg("Messages cleared");
        }
        elsif ($command eq 'messages')
        {
                foreach (@awaylogmessages)
                {
                        BouncerMsg($_);
                }
                
                if (@awaylogmessages)
                {
                        BouncerMsg("Use ?$config{client_bouncerCommand} clearmessages to clear these messages");
                }
                else
                {
                        BouncerMsg("No messages");
                }
        }
        elsif ($command eq 'playercount')
        {
                my $players = 0;
                my $playing = 0;
                
                foreach my $p (keys %server_players)
                {
                        $players += $server_players{$p}{enter_count};
                        $playing += $server_players{$p}{enter_count} if ($server_players{$p}{ship} != 8) # This number is not always correct, we can not tell the difference in ships between players with the same name 
                }
                
                BouncerMsg("Players: $players; Playing: $playing");
        }
        elsif ($command eq 'ignore' || $command eq 'ignoreirc') # todo, allow this to be sent to a player
        {
                if (length($args))
                {
                        $args = getChatnetNick($args) if ($command eq 'ignoreirc');
                        IgnorePlayer($args);
                        BouncerMsg("Ignoring: $args");
                }
                else
                {
                        BouncerMsg("You must specify a nickname to ignore");
                }
        }
        elsif ($command eq 'unignore' || $command eq 'unignoreirc')
        {
                if (length($args))
                {
                        $args = getChatnetNick($args) if ($command eq 'unignoreirc');
                        
                        if (exists $ignoreList{lc($args)})
                        {       
                                UnignorePlayer($args);
                                BouncerMsg("Listening: $args");
                        }
                        else
                        {
                                BouncerMsg("Player $args is not ignored");
                        }
                }
                else
                {
                        BouncerMsg("You must specify a nickname to unignore");
                }
        }
        elsif ($command eq 'reconnect')
        {
                BouncerMsg("Reconnecting...");
                RemoveSocket($server_sock); 
        }
        elsif ($command eq 'chat')
        {
                for ($a = 0; $a < scalar(@server_chats); $a++)
                {
                        BouncerMsg($server_chats[$a] . ": " . join(',', values %{$server_playersOnChat{$server_chats[$a]}}));
                }
        }
        elsif ($command eq 'eval' && $config{client_allowEval})
        {
                eval($args);
        }
        else
        {
                BouncerMsg("No such command: $command");
        }
}

sub BouncerMsg
{
        my ($msg) = @_;
        ToClient("MSG:SYSOP::[BOUNCER] $msg");
}

sub ClientLogMessage
{
        my ($nick, $msg) = @_;
        # Someone left a log message for the player
        
        $msg = Timestamp(). " $nick> $msg";
        
        push @awaylogmessages, $msg;
        
        return unless length($config{client_awayLogFile});
        if(!open (AWAY, '>>', $config{client_awayLogFile}))
        {
                l("Unable to write to $config{client_awayLogFile}");
                close(AWAY);
                return;
        }
        print AWAY "$msg\n";
        close AWAY;
}

sub ClearClientLogMessages
{
        @awaylogmessages = ();
        return unless length($config{client_awayLogFile});
        if(!open (AWAY, '>', $config{client_awayLogFile}))
        {
                l("Unable to write to $config{client_awayLogFile}");
                close(AWAY);
                return;
        }
        print AWAY "";
        close AWAY;
}

sub ReadClientLogMessagesFromFile
{
        return unless length($config{client_awayLogFile});
        my $line;
        
        if (!open(AWAY, '<', $config{client_awayLogFile}))
        {
                l("Unable to read $config{client_awayLogFile}");
                return;
        }
        
        @awaylogmessages = ();
        
        while ($line = <AWAY>)
        {
                chomp $line;
                if (length($line))
                {
                     push @awaylogmessages, $line;   
                }
        }
        
        close AWAY;
}

sub IgnorePlayer
{
        my ($player) = @_;
        $ignoreList{lc($player)} = $player;
        SaveIgnoreList();
}

sub UnignorePlayer
{
        my ($player) = @_;
        delete $ignoreList{lc($player)};
        SaveIgnoreList();
}

sub SaveIgnoreList
{
        return unless (length($config{client_ignoreList}));
        my $line;
        
        if (!open(IGNORE, '>', $config{client_ignoreList}))
        {
                l("Unable to save $config{client_ignoreList}");
                return;
        }
        
        foreach $line (values %ignoreList)
        {
                print IGNORE $line."\n";
        }
        
        close IGNORE;
}

sub ReadIgnoreList
{
        return unless (length($config{client_ignoreList}));
        my $line;
        @awaylogmessages = ();
        
        if (!open(IGNORE, '<', $config{client_ignoreList}))
        {
                l("Unable to read $config{client_ignoreList}");
                return;
        }
        
        %ignoreList = ();
        
        while ($line = <IGNORE>)
        {
                chomp $line;
                if (length($line))
                {
                     $ignoreList{lc($line)} = $line;  
                }
        }
        
        close IGNORE;
}


sub l
{
        # Log something
        my ($message) = @_;
        print ("$message\n");
}

sub w
{
        # Warning
        my ($message) = @_;
        Carp::cluck("$message\n");
}

sub fe
{
        # Fatal error
        my ($message) = @_;
        Carp::confess("$message\n");
        exit(1); #unreachable
}

sub Timestamp { # timestamp
        my @ts = localtime(time());
        return sprintf("[%02d/%02d/%02d %02d:%02d:%02d] ",
                                   $ts[3],$ts[4]+1,$ts[5]%100,$ts[2],$ts[1],$ts[0]);
}

sub Trim
{
        my ($string) = @_;
        return $string unless (defined($string));
        
        $string =~ s/^\s+//;
        $string =~ s/\s+$//;
        return $string;
}

# Irc does not support spaces in IRC nicks
# chatnet nick -> irc nick
sub ircNick
{
        # There is a flaw with this solution:
        # The nicknames "A_ B" and "A _B" end up with the same name in IRC
        # This however, is only a problem for cross zone / arena private messages. (Because of the createIrcNick function)
        # The :Chatnet Name: method can still be used to message a person in this rare event (if you know his chatnet name)

        my ($nick) = @_;
        $nick =~ s/\Q$ircSpace\E/\Q$ircSpace$ircSpace\E/g;
        $nick =~ s/\s/\Q$ircSpace\E/g;
        $nick =~ s/!/\xA1/g; # ISO 8859-1 upside down !
        return $nick;
        
#IRC RFC:       
#       letter     =  %x41-5A / %x61-7A       ; A-Z / a-z
#       digit      =  %x30-39   
#       special    =  %x5B-60 / %x7B-7D
#       nickname   =  ( letter / special ) *8( letter / digit / special / "-" )

}

# Create a new unique irc nick for a chatnet name. If the irc name already exists a number is appended to the name
sub createIrcNick
{
        my ($chatnetNick) = @_;
        my $chatnetNicklc = lc($chatnetNick);
        my $ircNick = ircNick($chatnetNick);
        my $ircNicklc = lc($ircNick);
        
        if (defined($usedIrcNicks{$ircNicklc}))
        {
                my $a = 2;
                
                while (defined($usedIrcNicks{$ircNicklc . $ircSpace . $a}))
                {
                        $a++;
                }
                
                $ircNick .= $ircSpace . $a;
                $ircNicklc .= $ircSpace . $a;
        }
        
        $usedIrcNicks{$ircNicklc} = $chatnetNick;
        return $ircNick;
}

# Free an irc nick
sub freeIrcNick
{
        my ($ircNick) = @_;
        $ircNick = lc($ircNick);
        
        delete $usedIrcNicks{$ircNick}
}

sub getIrcNick
{
        my ($chatnetNick, $noPlayerTable, $noPrefix) = @_;
        my $chatnetNicklc = lc($chatnetNick);
        
        $noPrefix = 1 unless ($config{client_remotePrefix});
        
        if (defined $server_players{$chatnetNicklc}{irc_nick} && !$noPlayerTable)
        {
                return $server_players{$chatnetNicklc}{irc_nick}; 
        }
        else # Not in this arena
        {
                return ($noPrefix ? '' : '*').ircNick($chatnetNick); # a * is used for players that are not in this arena
        } 
}

sub getChatnetNick
{
        my ($ircNick) = @_;
        my $ircNicklc = lc($ircNick);
        
        if (substr($ircNick, 0, 1) ne '*' && defined $usedIrcNicks{$ircNicklc})
        {
                return $usedIrcNicks{$ircNicklc};
        }
        else
        {
                $ircNick =~ s/^\*//; # Remove leading *
                $ircNick =~ s/__/\037/g; # Replace double _ with a temp character which can never occur in a nickname.
                $ircNick =~ s/_/ /g;     # Replace _ with a space
                $ircNick =~ s/\037/_/g; # Replace the temp character with a _
                $ircNick =~ s/\xA1/!/g;
                
                return $ircNick;
        }
}